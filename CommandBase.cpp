#include "CommandBase.h"

#include <Commands/Scheduler.h>

#include "Subsystems/DriveTrain.h"


DriveTrain* CommandBase::drivetrain = NULL;
OI* CommandBase::controls = NULL;

CommandBase::CommandBase() : frc::Command() {}

CommandBase::CommandBase(const char *name) :
		frc::Command(name) {
}

void CommandBase::Init() {
	drivetrain = NULL;
	controls = NULL;

	drivetrain = new DriveTrain();
	controls = new OI();
}
