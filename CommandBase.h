#ifndef COMMAND_BASE_H
#define COMMAND_BASE_H

#include <memory>
#include <string>

#include <Commands/Command.h>

#include "OI.h"
#include "Subsystems/DriveTrain.h"

class CommandBase: public frc::Command {
public:
	static DriveTrain *drivetrain;
	static OI *controls;

	CommandBase(const char *name);
	CommandBase();
	static void Init();
};

#endif  // COMMAND_BASE_H
