#include <memory>

#include <Commands/Command.h>
#include <Commands/Scheduler.h>
#include <IterativeRobot.h>
#include <LiveWindow/LiveWindow.h>
#include <SmartDashboard/SendableChooser.h>
#include <SmartDashboard/SmartDashboard.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "GripPipeline.h"

#include "Commands/TeleopDefault.h"
#include "CommandBase.h"

NetworkTable *table;

class Robot: public frc::IterativeRobot {
private:
	frc::SendableChooser<frc::Command*> chooser;
	std::unique_ptr<frc::Command> autonomousCommand;
	frc::Command* autonomousMode;
	static void VisionThread() {
		std::cout << "Initializing camera..." << std::endl;
		cs::UsbCamera camera = frc::CameraServer::GetInstance()->StartAutomaticCapture();
		camera.SetBrightness(20);
		if (camera.IsConnected()) {
			/*camera.SetResolution(640,480); //lower than 480p
			camera.SetFPS(60);
			cs::CvSink cvSink = CameraServer::GetInstance()->GetVideo();
			cs::CvSource outputStreamStd = CameraServer::GetInstance()->PutVideo("Color", 640, 480);
			cv::Mat source;
			cv::Mat output;
			while (true) {
				cvSink.GrabFrame(source);
				cvtColor(source, output, cv::COLOR_BGR2RGB);
				outputStreamStd.PutFrame(output);
			}*/
			/*while (true) {
				std::vector<double> arr = table->GetNumberArray("myContoursReport", llvm::ArrayRef<double>());
				for (unsigned int i = 0; i < arr.size(); i++) {
					std::cout << arr[i] << " ";
				}
				std::cout << std::endl;
				Wait(1);
			}*/
		}
	}
public:
	void RobotInit() override {
		std::thread visionThread(VisionThread);
		visionThread.detach();
		CommandBase::Init();
		chooser	.AddDefault("Default Auto", new TeleopDefault());
		// chooser.AddObject("My Auto", new MyAutoCommand());
		frc::SmartDashboard::PutData("gay", &chooser);
	}
	void AutonomousPeriodic() override {
		frc::Scheduler::GetInstance()->Run();
	}

	void TeleopInit() override {
		// This makes sure that the autonomous stops running when
		// teleop starts running. If you want the autonomous to
		// continue until interrupted by another command, remove
		autonomousCommand.release();
		autonomousCommand.reset(chooser.GetSelected());

		if (autonomousCommand.get() != nullptr) {
			autonomousCommand->Start();
		}
	}

	void TeleopPeriodic() override {
		frc::Scheduler::GetInstance()->Run();
	}

	void TestPeriodic() override {
		frc::LiveWindow::GetInstance()->Run();
	}

};

START_ROBOT_CLASS(Robot)
