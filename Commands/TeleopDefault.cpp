#include "TeleopDefault.h"
#include "WPILib.h"

TeleopDefault::TeleopDefault() {
	std::cout << "Test" << std::endl;
	Requires(drivetrain);
}

// Called just before this Command runs the first time
void TeleopDefault::Initialize() {
	drivetrain->KillDrive();

}

// Called repeatedly when this Command is scheduled to run
void TeleopDefault::Execute() {
	double AverageX = (controls->LeftStick->GetX() + controls->RightStick->GetX())/2;
	if (AverageX > 0.4 || AverageX < -0.4) {
		drivetrain->Strafe(AverageX/4);
	} else {
		drivetrain->Drive(controls->LeftStick->GetY()/2, controls->RightStick->GetY()/2);
	}
}

// Make this return true when this Command no longer needs to run execute()
bool TeleopDefault::IsFinished() {
	return false;
}

// Called once after isFinished returns true
void TeleopDefault::End() {
	drivetrain->KillDrive();
}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void TeleopDefault::Interrupted() {
	drivetrain->KillDrive();
}
