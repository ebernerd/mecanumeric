#ifndef TeleopDefault_H
#define TeleopDefault_H

#include "../CommandBase.h"

class TeleopDefault : public CommandBase {
public:
	TeleopDefault();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
};

#endif  // TeleopDefault_H
