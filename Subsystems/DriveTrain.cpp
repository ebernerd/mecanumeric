#include "DriveTrain.h"
#include "../RobotMap.h"
#include "CANTalon.h"
#include "WPILib.h"
DriveTrain::DriveTrain() : Subsystem("DriveTrain") {

	FrontLeft = new CANTalon(1);
	BackLeft = new CANTalon(2);
	BackRight = new CANTalon(3);
	FrontRight = new CANTalon(4);

}

void DriveTrain::Drive(double LeftSpeed, double RightSpeed) {
	this->FrontLeft->Set(-LeftSpeed);
	this->BackLeft->Set(-LeftSpeed);
	this->BackRight->Set(RightSpeed);
	this->FrontRight->Set(RightSpeed);
}

void DriveTrain::KillDrive() {
	this->FrontLeft->Set(0);
	this->BackLeft->Set(0);
	this->BackRight->Set(0);
	this->FrontRight->Set(0);
}

void DriveTrain::Strafe(double Speed) {
	this->FrontLeft->Set(Speed);
	this->BackLeft->Set(-Speed);
	this->BackRight->Set(-Speed);
	this->FrontRight->Set(Speed);
}

void DriveTrain::Angle(double Speed) {
	this->FrontLeft->Set(-Speed);
	this->BackRight->Set(Speed);
}
