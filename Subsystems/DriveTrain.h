#ifndef DriveTrain_H
#define DriveTrain_H

#include <Commands/Subsystem.h>
#include "WPILib.h"
#include "CANTalon.h"

class DriveTrain : public Subsystem {
private:
	CANTalon *FrontLeft, *FrontRight, *BackLeft, *BackRight;

public:
	DriveTrain();
	void Drive(double,double);
	void Strafe(double);
	void KillDrive();
	void Angle(double);
};

#endif  // DriveTrain_H
